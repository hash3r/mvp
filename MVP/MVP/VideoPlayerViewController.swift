//
//  VideoPlayerViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/7/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import MediaPlayer

class VideoPlayerViewController: UIViewController {
	
	var url: NSURL?
	
	var moviePlayer: MPMoviePlayerController?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		startPlayingVideo()
	}
	
	func videoHasFinishedPlaying(notification: NSNotification){
		
		print("Video finished playing")
		
		/* Find out what the reason was for the player to stop */
		let reason =
		notification.userInfo![MPMoviePlayerPlaybackDidFinishReasonUserInfoKey]
			as! NSNumber?
		
		if let theReason = reason {
			
			let reasonValue = MPMovieFinishReason(rawValue: theReason.integerValue)
			
			switch reasonValue!{
			case .PlaybackEnded:
				/* The movie ended normally */
				print("Playback Ended")
			case .PlaybackError:
				/* An error happened and the movie ended */
				print("Error happened")
			case .UserExited:
				/* The user exited the player */
				print("User exited")
			}
			
			print("Finish Reason = \(theReason)")
			stopPlayingVideo()
		}
		
	}
	
	func stopPlayingVideo() {
		
		if let player = moviePlayer{
			NSNotificationCenter.defaultCenter().removeObserver(self)
			player.stop()
			player.view.removeFromSuperview()
			self.dismissViewControllerAnimated(false, completion: nil)
		}
		
	}
	
	func videoThumbnailIsAvailable(notification: NSNotification){
		
		if let _ = moviePlayer{
			print("Thumbnail is available")
			
			/* Now get the thumbnail out of the user info dictionary */
			let thumbnail =
			notification.userInfo![MPMoviePlayerThumbnailImageKey] as? UIImage
			
			if let image = thumbnail{
				
				/* We got the thumbnail image. You can now use it here */
				print("Thumbnail image = \(image)")
				
			}
		}
		
	}
	
	func startPlayingVideo(){
		
		/* First let's construct the URL of the file in our application bundle
		that needs to get played by the movie player */
		let mainBundle = NSBundle.mainBundle()
		

		/* If we have already created a movie player before,
		let's try to stop it */
		if let _ = moviePlayer{
			stopPlayingVideo()
		}
		
		/* Now create a new movie player using the URL */
		moviePlayer = MPMoviePlayerController(contentURL: url)
		
		if let player = moviePlayer{
			
			/* Listen for the notification that the movie player sends us
			whenever it finishes playing */
			NSNotificationCenter.defaultCenter().addObserver(self,
				selector: "videoHasFinishedPlaying:",
				name: MPMoviePlayerPlaybackDidFinishNotification,
				object: nil)
			
			NSNotificationCenter.defaultCenter().addObserver(self,
				selector: "videoThumbnailIsAvailable:",
				name: MPMoviePlayerThumbnailImageRequestDidFinishNotification,
				object: nil)
			
			print("Successfully instantiated the movie player")
			
			/* Scale the movie player to fit the aspect ratio */
			player.scalingMode = .AspectFit
			player.view.frame = self.view.bounds

			view.addSubview(player.view)
			

			player.setFullscreen(true, animated: true)
			/* Let's start playing the video in full screen mode */
			player.play()
			player.controlStyle = MPMovieControlStyle.Fullscreen

			/* Capture the frame at the third second into the movie */
			let thirdSecondThumbnail = 3.0
			
			/* We can ask to capture as many frames as we
			want. But for now, we are just asking to capture one frame
			Ask the movie player to capture this frame for us */
			player.requestThumbnailImagesAtTimes([thirdSecondThumbnail],
				timeOption: .NearestKeyFrame)
			
		} else {
			print("Failed to instantiate the movie player")
		}
		
	}
	
}