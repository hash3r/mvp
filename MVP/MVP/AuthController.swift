//
//  AuthViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import Foundation
import FBSDKLoginKit
import FBSDKCoreKit
import Alamofire

public typealias AuthViewControllerSuccessBlock = () -> ()
public typealias AuthViewControllerFailureBlock = (NSError?) -> ()

class AuthViewController: BaseAuthViewController, UITextFieldDelegate {
	
	var successBlock: AuthViewControllerSuccessBlock!
	var failureBlock: AuthViewControllerFailureBlock!
	
	@IBOutlet weak var keyboardConstraint: NSLayoutConstraint!
	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var emailField: UITextField!
	@IBOutlet weak var passField: UITextField!
	
	@IBOutlet var textFieldPlaceholder: [UIView]!
	
	override func viewDidLoad() {
		super.viewDidLoad()

		let attributes = [NSForegroundColorAttributeName: UIColor(0x7a7a7a)]
		nameField.attributedPlaceholder = NSAttributedString(string:"name",
			attributes:attributes)
		emailField.attributedPlaceholder = NSAttributedString(string:"e-mail",
			attributes:attributes)
		passField.attributedPlaceholder = NSAttributedString(string:"password",
			attributes:attributes)
		// Do any additional setup after loading the view, typically from a nib.

		for placeholder in textFieldPlaceholder {
			placeholder.layer.borderWidth = 1
			placeholder.layer.borderColor = UIColor(0x353535).CGColor
		}
//		self.navigationItem.backBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_button"), style:UIBarButtonItemStyle.Plain, target:self, action:nil);
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back_button"), style:UIBarButtonItemStyle.Plain, target:self, action:"closeView");
		
	}
	
	override func viewWillAppear(animated:Bool) {
		super.viewWillAppear(animated)
		NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardNotification:"), name:UIKeyboardWillShowNotification, object: nil);
		NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardNotification:"), name:UIKeyboardWillHideNotification, object: nil);
	}
	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func keyboardNotification(notification: NSNotification) {
		let isShowing = notification.name == UIKeyboardWillShowNotification
		if let userInfo = notification.userInfo {
			let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue()
			let endFrameHeight = endFrame?.size.height ?? 0.0
			let duration:NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
			let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
			let animationCurveRaw = animationCurveRawNSN?.unsignedLongValue ?? UIViewAnimationOptions.CurveEaseInOut.rawValue
			let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
			self.scrollView.setContentOffset(CGPointMake(0, isShowing ? endFrameHeight/2 : 0.0), animated: true)
			UIView.animateWithDuration(duration,
				delay: NSTimeInterval(0),
				options: animationCurve,
				animations: { self.view.layoutIfNeeded() },
				completion: nil)
		}
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		if textField == nameField {
			textField.resignFirstResponder()
			emailField.becomeFirstResponder()
		} else if textField == emailField {
			textField.resignFirstResponder()
			passField.becomeFirstResponder()
		} else if textField == passField {
			passField.resignFirstResponder()
		}
		return true
	}
	
	func closeView(){
		self.navigationController?.popViewControllerAnimated(true)
	}
	
	@IBAction func createAccount(sender: UIButton) {
		if nameField.text.isEmpty || emailField.text.isEmpty || passField.text.isEmpty {
			var alert = UIAlertController(title: "Empty fields", message: "Please fill in all fields", preferredStyle: UIAlertControllerStyle.Alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
			}))
			self.presentViewController(alert, animated: true, completion: nil)
			return
		}

		var params = ["email":emailField.text, "full_name":nameField.text, "password":passField.text]
		Alamofire.request(Method.POST, "http://107.170.151.144/api/user", parameters: params)
		.responseJSON { (request, response, jsonObject, error) in
			if let response = response {
				if let xAuth: String = response.allHeaderFields["X-Authenticate"] as? String {
					NSUserDefaults.standardUserDefaults().setValue(xAuth, forKey: "X-Authenticate")
					NSUserDefaults.standardUserDefaults().setValue(self.emailField.text, forKey: "email")
					NSUserDefaults.standardUserDefaults().setValue(self.nameField.text, forKey: "full_name")
					NSUserDefaults.standardUserDefaults().setValue(self.passField.text, forKey: "password")
					
					self.successBlock()
					
				} else { self.failureBlock(error) }
			} else { self.failureBlock(error) }
		}

	}

	@IBAction func enterWithFacebook(sender: UIButton) {
		
		loginToFacebookWithSuccess(successBlock, failureBlock: failureBlock)
	}
	
	let facebookReadPermissions = ["public_profile", "email"]
	//Some other options: "user_about_me", "user_birthday", "user_hometown", "user_likes", "user_interests", "user_photos", "friends_photos", "friends_hometown", "friends_location", "friends_education_history"
 
	func loginToFacebookWithSuccess(successBlock: () -> (), failureBlock: (NSError?) -> ()) {
		
		if FBSDKAccessToken.currentAccessToken() != nil {
			//For debugging, when we want to ensure that facebook login always happens
			//FBSDKLoginManager().logOut()
			//Otherwise do:
//			return
		}
		
		FBSDKLoginManager().logInWithReadPermissions(self.facebookReadPermissions, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
			if error != nil {
				//According to Facebook:
				//Errors will rarely occur in the typical login flow because the login dialog
				//presented by Facebook via single sign on will guide the users to resolve any errors.
				
				// Process error
				FBSDKLoginManager().logOut()
				failureBlock(error)
			} else if result.isCancelled {
				// Handle cancellations
				FBSDKLoginManager().logOut()
				failureBlock(nil)
			} else {
				// If you ask for multiple permissions at once, you
				// should check if specific permissions missing
				var allPermsGranted = true
				
				//result.grantedPermissions returns an array of _NSCFString pointers
				let grantedPermissions = Array(result.grantedPermissions).map( {"\($0)"} )
				for permission in self.facebookReadPermissions {
					if !contains(grantedPermissions, permission) {
						allPermsGranted = false
						break
					}
				}
				if allPermsGranted {
					// Do work
					let fbToken = result.token.tokenString
					let fbUserID = result.token.userID
					
					let userRequest = FBSDKGraphRequest(graphPath:"/me", parameters:["fields":"name, email"], HTTPMethod:"GET")
					userRequest.startWithCompletionHandler({ (connection, result, error: NSError!) -> Void in
						if error == nil {
							println("\(result)")
							if let name = result["name"] as? String  {
								NSUserDefaults.standardUserDefaults().setValue(name, forKey: "full_name")
								println("\(name)")
							}
							if let email = result["email"] as? String  {
								NSUserDefaults.standardUserDefaults().setValue(email, forKey: "email")
								println("\(email)")
							}
						} else {
							println("\(error)")
						}
					})
					
					//Send fbToken and fbUserID to your web API for processing, or just hang on to that locally if needed
					//self.post("myserver/myendpoint", parameters: ["token": fbToken, "userID": fbUserId]) {(error: NSError?) ->() in
					//	if error != nil {
					//		failureBlock(error)
					//	} else {
					//		successBlock(maybeSomeInfoHere?)
					//	}
					//}
					
					successBlock()
				} else {
					//The user did not grant all permissions requested
					//Discover which permissions are granted
					//and if you can live without the declined ones
					
					failureBlock(nil)
				}
			}
		})
	}

}