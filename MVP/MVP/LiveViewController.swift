//
//  LiveViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import HCYoutubeParser

let headerHeight: CGFloat = 9

class LiveViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
	
	@IBOutlet var tableView: UITableView!
	
//	var channels = Array<JSON>()
	// displayed programs
//	var programs = Array<JSON>()
//	var upcomings = Dictionary<String, JSON>()
	
	// current date
	var displayedDate = NSDate()
	
	var itemsCountToLoadInSingleRequest = 20
	var isTableViewInitialized = false
	var isLoading = false {
		didSet {
//			delegate.liveTVActivityIndication(isLoading)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Live"
		tableView.delegate = self
		tableView.dataSource = self
		tableView.separatorColor = UIColor.clearColor()
		tableView.estimatedRowHeight = 90
		tableView.rowHeight = UITableViewAutomaticDimension
		
//		let view = UIView(frame: CGRectMake(0, 0, tableView.frame.width, 64))
//		view.backgroundColor = UIColor.blackColor()
//		
//		self.tableView.tableHeaderView = view
	}
	
	
	// MARK: - Table view data source
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return section == 0 ? 0 : headerHeight
	}

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return liveTeams.count
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//		isTableViewInitialized = true
//		return programs.count
		return 1
	}
	
	let liveTeams = ["Warriors - Cavaliers", "Juventus - Barcelona", "Latvia - Netherlands"]
	let liveTypes = ["Basketball: NBA", "Soccer: UEFA Champions League", "Soccer: UEFA European Qualifiers"]
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("LiveCell", forIndexPath: indexPath) as! LiveCell
		cell.teamsLabel.font = UIFont(name:"Sansus Webissimo", size:17)
		cell.bgImage.image = UIImage(named: "live_cell_\(indexPath.section+1)")
//		cell.separatorInset = UIEdgeInsetsMake(10.0, 10, 10.0, 10.0)
		cell.selectionStyle = UITableViewCellSelectionStyle.None
		cell.teamsLabel.text = liveTeams[indexPath.section]
		cell.gameTypeLabel.text = liveTypes[indexPath.section]
//		cell.channelImage.hidden = false
//		cell.clipsToBounds = true
		return cell
	}
	
	func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

	}
	
	@IBAction func playVideo(sender: UIButton) {
		let cell = sender.superview!.superview as! LiveCell
		let indexPath = self.tableView.indexPathForCell(cell)
		var vc = VideoPlayerViewController()
		vc.view.frame = UIScreen.mainScreen().bounds
		if indexPath!.section == 0 {
//			basket
			if let video = HCYoutubeParser.h264videosWithYoutubeURL(VideoLinkManager.sharedInstance.linkForSportType(SportType.Basketball)) {
//			vc.url = NSURL(string: "http://107.170.151.144/api/videos/11/video_file_one_off/6753c27db92a9b7065ec8022cf90aab4eb8f9b3a")!
				if let mediumUrl = video["medium"] as? String {
					vc.url = NSURL(string:mediumUrl)!
				}
			}
		} else {
//			foot
			if let video = HCYoutubeParser.h264videosWithYoutubeURL(VideoLinkManager.sharedInstance.linkForSportType(SportType.Soccer)) {
				if let mediumUrl = video["medium"] as? String {
					vc.url = NSURL(string:mediumUrl)!
				}
			}
		}
		if let url = vc.url {
			self.presentViewController(vc, animated: false, completion: nil)
		} else {
			var alert = UIAlertController(title: "Wrong URL", message: "This video is not available", preferredStyle: UIAlertControllerStyle.Alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
			}))
			self.presentViewController(alert, animated: true, completion: nil)
		}
	}
	
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

	}
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, headerHeight))
		headerView.backgroundColor = UIColor.clearColor()
		return headerView
	}
}

class LiveCell : UITableViewCell {
	
	@IBOutlet weak var bgImage: UIImageView!
	
	@IBOutlet weak var teamsLabel: UILabel!
//	@IBOutlet weak var timeLabel: UILabel!
//	@IBOutlet weak var channelName: UILabel!
	@IBOutlet weak var channelImage: UIImageView!
	@IBOutlet weak var gameTypeLabel: UILabel!
//	@IBOutlet weak var synopsis: UILabel!
//	@IBOutlet weak var gradientImage: UIImageView!
	@IBOutlet weak var teamLeftImage: UIImageView!
	@IBOutlet weak var teamRightImage: UIImageView!
//	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	var indexPath: NSIndexPath!
	var programId: String?
	
	override func awakeFromNib() {
		bgImage.layer.masksToBounds = true
	}
	
//	@IBAction func playButtonTapped(sender: AnyObject) {
////		TODO: play video
//	}
	
//	/**
//	UI modification when there is no background image
//	*/
//	func handleNoBG() {
//		self.bgImage.image = UIImage(named: "whats_on_default")
//		self.channelImage.hidden = true
//		self.channelName.hidden = true
//		self.largeChannelImage.hidden = false
//		self.gradientImage.hidden=true
//	}
//	
//	var channel: JSON!
//	
//	func updateChannelInfo(channel: JSON) {
//		self.channel = channel
//		self.channelName.text = channel["stationInfo"]["onScreenCallSign"].string
//		ImageDownloadQueue.instance.addOperationWithBlock({ () -> Void in
//			for (key: String, selectedImage: JSON) in channel["stationInfo"]["selectedImages"] {
//				if selectedImage["alias"] == "thumb" {
//					if  let stringURL = selectedImage["url"].string{
//						self.channelImage.contentMode = .ScaleAspectFit
//						self.channelImage.HC_SetImageWithURL(stringURL)
//						self.largeChannelImage.HC_SetImageWithURL(stringURL)
//					}
//					break
//				}
//			}
//		})
//	}

}

