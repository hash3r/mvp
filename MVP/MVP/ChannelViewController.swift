//
//  ChannelViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/1/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

enum ChannelsType {
	case AFN
	case MLB
	case NHL
	case OUTDOOR
	case NBA
	case ONE
	case WILLOW
}

struct Channel {
	let type: ChannelsType
	
	init(type : ChannelsType) {
		self.type = type
	}
	
	func icon() -> UIImage? {
		switch self.type {
			case .AFN: return UIImage(named: "ch_icon_1");
			case .MLB: return UIImage(named: "ch_icon_7");
			case .NHL: return UIImage(named: "ch_icon_5");
			case .OUTDOOR: return UIImage(named: "ch_icon_8");
			case .NBA: return UIImage(named: "ch_icon_6");
			case .ONE: return UIImage(named: "ch_one_icon");
			case .WILLOW: return UIImage(named: "willow_icon");
		}
	}
	
	func videoImage() -> UIImage? {
		switch self.type {
		case .AFN: return UIImage(named: "ch_curr_cell");
		case .MLB: return UIImage(named: "mlb");
		case .NHL: return UIImage(named: "nhl");
		case .OUTDOOR: return UIImage(named: "outdoor");
		case .NBA: return UIImage(named: "ch_curr_cell");
		case .ONE: return UIImage(named: "one_current");
		case .WILLOW: return UIImage(named: "cricket_image");
		}
	}
	
	func videoDescription() -> String {
		switch self.type {
		case .AFN: return "";
		case .MLB: return "Bell hits walk-off blast for Altoona";
		case .NHL: return "Pittsburgh Penguins vs Minnesota Wild";
		case .OUTDOOR: return "Monster Fish TV filmed on the Niagara River";
		case .NBA: return "";
		case .ONE: return "Manchester United vs Tottenham Hotspur";
		case .WILLOW: return "India vs Srilanka World Cup Final";
		}
	}
	
	func name() -> String {
		switch self.type {
			case .AFN: return "AFN";
			case .MLB: return "MiLB";
			case .NHL: return "NHL";
			case .OUTDOOR: return "OUTDOOR";
			case .NBA: return "NBA TV";
			case .ONE: return "ONE";
			case .WILLOW: return "WILLOW";
		}
	}
	
	func description() -> String {
		switch self.type {
			case .AFN: return "American Forces Network";
			case .MLB: return "Minor League Baseball";
			case .NHL: return "NHL";
			case .OUTDOOR: return "America's Leader in Outdoor TV";
			case .NBA: return "NBA TV";
			case .ONE: return "One World Sports";
			case .WILLOW: return "Willow is official destination in North America for top International cricket played worldwide";
		}
	}
}

class ChannelViewController : BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {

	@IBOutlet weak var collectionView: UICollectionView!
	
	let channels = [
		Channel(type: ChannelsType.AFN),
		Channel(type: ChannelsType.MLB),
		Channel(type: ChannelsType.NHL),
		Channel(type: ChannelsType.OUTDOOR),
		Channel(type: ChannelsType.NBA),
		Channel(type: ChannelsType.ONE),
		Channel(type: ChannelsType.WILLOW)
	]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Channels"
		
		let kCellsPerRow: CGFloat = 2
		if let flowLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
			let availableWidthForCells: CGFloat = self.view.frame.size.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right - flowLayout.minimumInteritemSpacing * (kCellsPerRow - 1);
			let cellWidth: CGFloat = availableWidthForCells / kCellsPerRow;
			flowLayout.itemSize = CGSizeMake(cellWidth, cellWidth);
		}
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return channels.count
	}
	
	// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ChannelCell", forIndexPath: indexPath) as! ChannelCell
		cell.channelIcon.image = channels[indexPath.item].icon()
		return cell
	}
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		var vc = storyboard.instantiateViewControllerWithIdentifier("ChannelDetailsViewController") as! ChannelDetailsViewController
		vc.channelIndex = indexPath.item
		vc.channel = channels[indexPath.item]
		self.navigationController?.pushViewController(vc, animated: true)
	}

}

class ChannelCell: UICollectionViewCell {
	
	@IBOutlet weak var channelIcon: UIImageView!
}