//
//  FirstViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import Async

class FirstViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(true, animated: false)
	}

	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)
		self.navigationController?.setNavigationBarHidden(false, animated: false)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "authPush" {
			let vc:AuthViewController = segue.destinationViewController as! AuthViewController
			vc.successBlock = {
				let storyboard = UIStoryboard(name: "Main", bundle: nil)
				var vc = storyboard.instantiateViewControllerWithIdentifier("SuccessRegisteredViewController") as! SuccessRegisteredViewController
				self.navigationController?.pushViewController(vc, animated: true)
			}
			vc.failureBlock = {error in
				var alert = UIAlertController(title: "Can not register", message: "Please fill in valid data", preferredStyle: UIAlertControllerStyle.Alert)
				alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
				}))
				let delay = 0.5 * Double(NSEC_PER_SEC)
				Async.main(after: delay) {
					self.presentViewController(alert, animated: true, completion: nil)
				}
			}
		}
	}

}

