//
//  SearchViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/1/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

class SearchViewController : BaseViewController, UITextFieldDelegate {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Search"
	}
		
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		return true
	}
}