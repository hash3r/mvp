//
//  SportDetailsViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/1/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import MediaPlayer
import HCYoutubeParser

let sport_screens = ["sp_basket_1",	"sp_nfl", "sp_baseball", "sp_nhl_1", "sp_soccer", "sp_golf", "sp_tennis", "sp_boxing", "sp_cycling", "sp_moto", "cricket_image"]
let sport_labels = ["Basketball: Rockets vs. Suns", "NFL: Miami Dolphins vs. Washington Redskins", "Baseball: Boston Red Sox vs. Tigers", "NHL: NY Rangers vs. Chicago Blackhawks", "Soccer: Fluminense vs. Cruzeiro", "Golf: Barbasol Championship", "Tennis: Rebecca Peterson vs. Mandy Minella  ", "Boxing: Scott Quigg vs. Nonito Donaire", "Cycling: Giro Rosa", "Motorcycling: ATV Motocross National Championship", "Cricket: India vs Srilanka World Cup Final"]
let sport_upcoming = [["up_basketball_1", "up_basketball_2", "up_basketball_3"], ["up_nfl_1", "up_nfl_2", "up_nfl_3"], ["up_base_1", "up_base_2", "up_base_3"]]

class SportDetailsViewController : BaseViewController {
	
	var	selectedSport: SportType?
//	var sportIndex: Int = 0
	var selectedTab: Int = 0
	var datasource = [String]()
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var tabBarImage: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = selectedSport?.description
		if selectedSport!.rawValue < sport_upcoming.count {
			datasource = sport_upcoming[selectedSport!.rawValue]
		}
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		updateFlowLayout()
	}
	
	func updateFlowLayout() {
		var flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
		flowLayout.itemSize = selectedTab == 0 ? CGSizeMake(314, 177) : CGSizeMake(314, 130)
		if DeviceType.IS_IPHONE_6 {
			flowLayout.itemSize = CGSizeMake(flowLayout.itemSize.width * 1.17, flowLayout.itemSize.height * 1.17)
		} else if DeviceType.IS_IPHONE_6P {
			flowLayout.itemSize = CGSizeMake(flowLayout.itemSize.width * 1.96, flowLayout.itemSize.height * 1.96)
		}
	}
	
	@IBAction func tabBarButtonTapped(sender: UIButton) {
		tabBarImage.image = UIImage(named: "sp_tab_\(sender.tag)")
		selectedTab = sender.tag - 1
		updateFlowLayout()
		collectionView.reloadData()
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return selectedTab == 0 ? 1 : datasource.count
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("SportDetailsCell", forIndexPath: indexPath) as! SportDetailsCell
		if selectedTab == 0 {
			cell.bgImage.image = UIImage(named: sport_screens[selectedSport!.rawValue])
			cell.sportLabel.text = sport_labels[selectedSport!.rawValue]
			cell.playButton.hidden = false
			cell.remindButton.hidden = true
		} else {
			cell.bgImage.image = UIImage(named: datasource[indexPath.row])
			cell.sportLabel.text = ""
			cell.playButton.hidden = true
			cell.remindButton.hidden = false
		}
		return cell
	}
	
	
	@IBAction func playVideo(sender: AnyObject) {
		var vc = VideoPlayerViewController()
		vc.view.frame = UIScreen.mainScreen().bounds

		if let video = HCYoutubeParser.h264videosWithYoutubeURL(VideoLinkManager.sharedInstance.linkForSportType(selectedSport!)) {
			if let mediumUrl = video["medium"] as? String {
				vc.url = NSURL(string:mediumUrl)!
			}
		}
		if let url = vc.url {
			self.presentViewController(vc, animated: false, completion: nil)
		} else {
			var alert = UIAlertController(title: "Wrong URL", message: "This video is not available", preferredStyle: UIAlertControllerStyle.Alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
			}))
			self.presentViewController(alert, animated: true, completion: nil)
		}
	}

	@IBAction func remindButtonTapped(sender: UIButton) {
		sender.selected = !sender.selected
	}
}

//class FlowLayout : UICollectionViewFlowLayout {
//
//	override func prepareLayout() {
//		super.prepareLayout()
//		let contentSize = self.collectionView!.contentSize;
//	// Without this line, the scrolling doesn't go all the way to the bottom, right away but takes a while
//		super.layoutAttributesForElementsInRect(CGRectMake(0.0, 0.0, contentSize.width, contentSize.height))
//	}
//}

class SportDetailsCell: UICollectionViewCell {
	var moviePlayer:MPMoviePlayerController!
	@IBOutlet weak var bgImage: UIImageView!
	@IBOutlet weak var sportLabel: UILabel!
	@IBOutlet weak var playButton: UIButton!
	@IBOutlet weak var remindButton: UIButton!
	
}
