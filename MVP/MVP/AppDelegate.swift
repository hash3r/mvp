//
//  AppDelegate.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Fabric
import Crashlytics


let authOn = true

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		// Override point for customization after application launch.
		FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

		window = UIWindow(frame:UIScreen.mainScreen().bounds)
//		fontList()
		setupAppearance()
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		if authOn {
			if isAuthorized() == false {
				setupAuthController()
			} else {
				var masterViewController = storyboard.instantiateViewControllerWithIdentifier("MasterViewController") as! MasterViewController
				window?.rootViewController = masterViewController
			}
		} else {
			var masterViewController = storyboard.instantiateViewControllerWithIdentifier("MasterViewController") as! MasterViewController
			window?.rootViewController = masterViewController
		}
		window?.makeKeyAndVisible()
		Fabric.with([Crashlytics()])
		
		VideoLinkManager.sharedInstance.readContent()
		
		return true
	}
	
	func isAuthorized() -> Bool {
		if let xauth = NSUserDefaults.standardUserDefaults().stringForKey("X-Authenticate") {
			return true
		} else if FBSDKAccessToken.currentAccessToken() != nil {
			return true
		}
		return false
	}
	
	func setupAuthController() {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		var authNavController = storyboard.instantiateViewControllerWithIdentifier("FirstNavController") as! UINavigationController
		window?.rootViewController = authNavController
	}
	
	func setupAppearance() {
//		let back_button = UIImage(named: "nack_button")?.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 25, 0, 0))
//		UIBarButtonItem.appearance().setBackButtonBackgroundImage(back_button, forState: UIControlState.Normal, barMetrics: UIBarMetrics.Default)
		UINavigationBar.appearance().barTintColor = UIColor(0xff3b00)
		UINavigationBar.appearance().tintColor = UIColor.whiteColor()
		if DeviceType.IS_IPHONE_5 {
//			UITabBar.appearance().backgroundImage = UIImage(named:"tab_bar_back")
		}
		UITabBar.appearance().tintColor = UIColor(0xff3b00)
		UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name:"Sansus Webissimo", size:17)!, NSForegroundColorAttributeName: UIColor.whiteColor()], forState: UIControlState.Normal)
		UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name:"MyriadPro-Regular", size:17)!, NSForegroundColorAttributeName: UIColor(0xff3b00)], forState: UIControlState.Selected)
		UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
	}
	
	func fontList() {
		for family in UIFont.familyNames()
		{
			println(family)
			for name in UIFont.fontNamesForFamilyName(family as! String)
			{
				println(name)
			}
		}
	}

	func applicationWillResignActive(application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
		FBSDKAppEvents.activateApp()
	}

	func applicationWillTerminate(application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
		//Even though the Facebook SDK can make this determinitaion on its own,
		//let's make sure that the facebook SDK only sees urls intended for it,
		//facebook has enough info already!
		let isFacebookURL = url.scheme != nil && url.scheme!.hasPrefix("fb\(FBSDKSettings.appID())") && url.host == "authorize"
		if isFacebookURL {
			return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
		}
		return false
	}


}

