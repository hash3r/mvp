//
//  SuccessRegisteredViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/15/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

class SuccessRegisteredViewController : BaseAuthViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
	}

	@IBAction func continueRegistration(sender: UIButton) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		var vc = storyboard.instantiateViewControllerWithIdentifier("FavoriteTeamsViewController") as! FavoriteTeamsViewController
		self.navigationController?.pushViewController(vc, animated: true)
	}
}
