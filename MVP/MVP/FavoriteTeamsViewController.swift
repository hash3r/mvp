//
//  FavoriteTeamsViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/16/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Dollar

class SelectedTeam: NSObject {
	var team: Team?
	var button: UIButton
	var index: Int
	
	init(team: Team?, button: UIButton, index: Int) {
		self.team = team
		self.button = button
		self.index = index
	}
}

class Team: NSObject {
	let type: SportType
	let name: String
	let image: String
	var selected = false
	
	init(type: SportType, name : String, image : String) {
		self.type = type
		self.name = name
		self.image = image
	}
}

enum SportType: Int, Printable {
	case Basketball
	case NFL
	case Baseball
	case NHL
	case Soccer
	case Golf
	case Tennis
	case Boxing
	case Cycling
	case Motorcycling
	case Cricket
	
	var description: String {
		switch self {
			case .Basketball: return "Basketball";
			case .NFL: return "NFL";
			case .Baseball: return "Baseball";
			case .NHL: return "NHL";
			case .Soccer: return "Soccer";
			case .Golf: return "Golf";
			case .Tennis: return "Tennis";
			case .Boxing: return "Boxing";
			case .Cycling: return "Cycling";
			case .Motorcycling: return "Motorcycling";
			case .Cricket: return "Cricket";
		}
	}
}

let sportTypes = ["Please select sport type", SportType.Basketball.description, SportType.NFL.description, SportType.Baseball.description, SportType.NHL.description, SportType.Soccer.description, SportType.Golf.description, SportType.Tennis.description, SportType.Boxing.description, SportType.Cycling.description, SportType.Motorcycling.description, SportType.Cricket.description]


class FavoriteTeamsViewController : BaseAuthViewController, UICollectionViewDataSource, UICollectionViewDelegate {
	
	@IBOutlet var favoriteLines: [UIImageView]!
	@IBOutlet var favoriteButtons: [UIButton]!
	@IBOutlet weak var sportTypeButton: UIButton!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var nextTeamButton: UIButton!
	
	var collectionDataSource = [Team]()
	var selectedSportIndex: Int = 0
	var selectedButtonIndex: Int = 0
	var selectedTeams:[SelectedTeam] = []
	
	let datasource: [Array<Team>] = [
		[],
		[Team(type: SportType.Basketball, name:"Golden State Warriors", image: "fav_team_1"),
			Team(type: SportType.Basketball, name:"Cavaliers", image: "fav_team_2"),
			Team(type: SportType.Basketball, name:"Chicago Bulls", image: "fav_team_3"),
			Team(type: SportType.Basketball, name:"Los Angeles Lakers", image: "fav_team_4")],
		[Team(type: SportType.NFL, name:"Houston Texans", image: "fav_team_5"),
			Team(type: SportType.NFL, name:"Dallas Cowboys", image: "fav_team_6"),
			Team(type: SportType.NFL, name:"Denver Broncos", image: "fav_team_7"),
			Team(type: SportType.NFL, name:"Tennessee Titans", image: "fav_team_8")],
		[Team(type: SportType.Baseball, name:"Red Wings", image: "fav_team_9"),
			Team(type: SportType.Baseball, name:"Baltimore Orioles", image: "fav_team_10"),
			Team(type: SportType.Baseball, name:"Houston Astros", image: "fav_team_11"),
			Team(type: SportType.Baseball, name:"Twins", image: "fav_team_12")]
	]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		selectedTeams = [
			SelectedTeam(team: nil, button: favoriteButtons[0], index: 0),
			SelectedTeam(team: nil, button: favoriteButtons[1], index: 0),
			SelectedTeam(team: nil, button: favoriteButtons[2], index: 0),
			SelectedTeam(team: nil, button: favoriteButtons[3], index: 0),
		]
		
		sportTypeButton.layer.borderWidth = 0.5
		sportTypeButton.layer.borderColor = UIColor(0x353535).CGColor

		for button in favoriteButtons {
			button.layer.borderWidth = 0.5
			button.layer.borderColor = UIColor(0x353535).CGColor
		}
		let button = favoriteButtons.first
		savedTeamButtonTapped(button!)
	}
	
	@IBAction func savedTeamButtonTapped(sender: UIButton) {
		
//		if selectedTeams.count < sender.tag - 1 {
//			var selectedTeam = selectedTeams[sender.tag - 1]
//			self.sportTypeButton.titleLabel!.text = "Please select sport type"
//			self.sportTypeButton.imageView?.image = UIImage()
//			self.collectionDataSource = nil
//			self.collectionView.reloadData()
//		} else {
		
		selectedButtonIndex = sender.tag - 1

		
		for button in favoriteButtons {
			button.selected = false
		}
		for label in favoriteLines {
			label.hidden = true
		}
		sender.selected = true
		var label = favoriteLines[selectedButtonIndex]
		label.hidden = false
		
		var selectedTeam = selectedTeams[selectedButtonIndex]

		selectedSportIndex = selectedTeam.index
		sportTypeButton.setTitle(sportTypes[selectedSportIndex], forState: .Normal)
		sportTypeButton.setImage(UIImage(named: "sp_icon_\(selectedSportIndex)"), forState: .Normal)
		collectionDataSource = (selectedSportIndex < datasource.count) ? datasource[selectedSportIndex] : []
		collectionView.reloadData()
	}
	
	@IBAction func sportTypeTapped(sender: UIButton) {
		ActionSheetStringPicker.showPickerWithTitle("Select sport type", rows: sportTypes, initialSelection: 0, doneBlock: {
			picker, index, valuein in
			self.selectedSportIndex = index
			self.sportTypeButton.setTitle(sportTypes[index], forState: .Normal)
			self.sportTypeButton.setImage(UIImage(named: "sp_icon_\(index)"), forState: .Normal)
			self.collectionDataSource = index < self.datasource.count ? self.datasource[index] : []
			if (self.collectionDataSource.isEmpty == false) {
				var differ = [Team]()
				for selectedTeam in self.selectedTeams {
					if (selectedTeam.team != nil) {
						differ.append(selectedTeam.team!)
					}
				}
				//			differ = $.compact(differ)
				self.collectionDataSource = $.difference(self.collectionDataSource, differ)
			}

			self.collectionView.reloadData()

//			println("value = \(value)")
			println("index = \(index)")
//			println("picker = \(picker)")
			return
			}, cancelBlock: { ActionStringCancelBlock in return }, origin: sender)
	}

	@IBAction func teamSelected(sender: UIButton) {
		if let cell = sender.superview?.superview as? FavoriteCell {
			if let indexPath = self.collectionView.indexPathForCell(cell) {
				let team = collectionDataSource[indexPath.row]
					
					$.each(collectionDataSource) {
						$0.selected = false
					}
					
					team.selected = !team.selected
					cell.selectedImage.hidden = !cell.selectedImage.hidden
					var selectedTeam = selectedTeams[selectedButtonIndex]
					selectedTeam.team = team
					selectedTeam.index = selectedSportIndex
					selectedTeam.button = favoriteButtons[selectedSportIndex]
//					finishRegistration(sender)
					self.collectionView.reloadData()
					
					if selectedButtonIndex == favoriteButtons.count - 1 {
						nextTeamButton.setTitle("Finish registration", forState: .Normal)
					}
//				}
			}
		}
	}
	
	@IBAction func finishRegistration(sender: UIButton) {
		if selectedButtonIndex != favoriteButtons.count - 1 {
			let button = favoriteButtons[selectedButtonIndex + 1]
			savedTeamButtonTapped(button)
		} else {
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			var masterViewController = storyboard.instantiateViewControllerWithIdentifier("MasterViewController") as! MasterViewController
			self.showViewController(masterViewController, sender: self)
		}
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
			var size = CGSizeMake(150, 205)
			if DeviceType.IS_IPHONE_6 {
				size = CGSizeMake(size.width * 1.17, size.height * 1.17)
			} else if DeviceType.IS_IPHONE_6P {
				size = CGSizeMake(size.width * 1.94, size.height * 1.94)
			}
			return size
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.collectionDataSource.count
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("favoriteCell", forIndexPath: indexPath) as! FavoriteCell
		cell.team = collectionDataSource[indexPath.row]
		cell.selectTeamButton.layer.borderWidth = 0.5
		cell.selectTeamButton.layer.borderColor = UIColor(0x353535).CGColor
		return cell
	}
	
}

class FavoriteCell: UICollectionViewCell {
	
	var team: Team? {
		didSet {
			if let team = team {
				selectedImage.hidden = !team.selected
				teamLabel.text = team.name
				teamImage.image = UIImage(named: team.image)
			}
		}
	}
	
	@IBOutlet weak var selectedImage: UIImageView!
	@IBOutlet weak var teamLabel: UILabel!
	@IBOutlet weak var teamImage: UIImageView!
	@IBOutlet weak var selectTeamButton: UIButton!
}