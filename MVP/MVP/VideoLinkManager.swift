//
//  VideoLinkManager.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 8/4/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import Foundation

class VideoLinkManager: NSObject {
	static let sharedInstance = VideoLinkManager()
	var sportLinks: [String]?
	var channelLinks: [String]?
	
	override init() {
		super.init()
	}
	
	func readContent() {
		var error: NSError?
		
		let sportContent = String(contentsOfURL: NSURL(string: "https://www.dropbox.com/s/jqgb87iaikdkphd/mvp_links.txt?dl=1")!, encoding: NSASCIIStringEncoding, error: &error)
		if let content = sportContent {
			if let error = error {
				println("read content failure: \(error.localizedDescription)")
				return
			}
			sportLinks = content.componentsSeparatedByString("\n")
		}
		

		let channelContent = String(contentsOfURL: NSURL(string: "https://www.dropbox.com/s/805qge9kfhi1qk4/channel_links.txt?dl=1")!, encoding: NSASCIIStringEncoding, error: &error)
		if let content = channelContent {
			if let error = error {
				println("read content failure: \(error.localizedDescription)")
				return
			}
			channelLinks = content.componentsSeparatedByString("\n")
		}
	}
	
	func linkForSportType(sportType: SportType) -> NSURL? {
		if let links = sportLinks {
			switch sportType {
				case .Basketball: return NSURL(string: links[0]);
				case .NFL: return NSURL(string: links[1]);
				case .Baseball: return NSURL(string: links[2]);
				case .NHL: return NSURL(string: links[3]);
				case .Soccer: return NSURL(string: links[4]);
				case .Golf: return NSURL(string: links[5]);
				case .Tennis: return NSURL(string: links[6]);
				case .Boxing: return NSURL(string: links[7]);
				case .Cycling: return NSURL(string: links[8]);
				case .Motorcycling: return NSURL(string: links[9]);
				case .Cricket: return NSURL(string: links[10]);
			}
		}
		return nil
	}
	
	func linkForChannelType(channelType: ChannelsType) -> NSURL? {
		if let links = channelLinks {
			switch channelType {
				case .AFN: return NSURL(string: links[0]);
				case .MLB: return NSURL(string: links[1]);
				case .NHL: return NSURL(string: links[2]);
				case .OUTDOOR: return NSURL(string: links[3]);
				case .NBA: return NSURL(string: links[4]);
				case .ONE: return NSURL(string: links[5]);
				case .WILLOW: return NSURL(string: links[6]);
			}
		}
		return nil
	}
}