//
//  BaseAuthViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/16/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

class BaseAuthViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		var titleLabel = UILabel(frame: CGRectMake(0, 0, 60, 40))
		titleLabel.textAlignment = NSTextAlignment.Center
		titleLabel.font = UIFont(name: "Exo2-BoldItalic", size: 28)
		titleLabel.textColor = UIColor.whiteColor()
		titleLabel.text = "MVP"
		navigationItem.titleView = titleLabel
	}
}
