//
//  MasterViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/2/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import Dollar
import Alamofire

class MasterViewController: UIViewController {
	
	@IBOutlet weak var tabView: UIView!
	@IBOutlet weak var tabViewHeight: NSLayoutConstraint!
	@IBOutlet weak var tabImageView: UIImageView!
	@IBOutlet var containerViews: [UIView]!
	@IBOutlet var tabButtons: [UIButton]!
	
	@IBOutlet weak var liveButton: UIButton!
	@IBOutlet weak var sportButton: UIButton!
	@IBOutlet weak var channelButton: UIButton!
	@IBOutlet weak var searchButton: UIButton!
	@IBOutlet weak var settingsButton: UIButton!

	@IBOutlet var buttonWidth: [NSLayoutConstraint]!
	@IBOutlet weak var shortButtonWidth: NSLayoutConstraint!
	
	var activeContainer: UIView!
	var liveVCLoaded = false
	var sportVCLoaded = false
	var channelVCLoaded = false
	var searchVCLoaded = false
	var settingsVCLoaded = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		fetchVideoLinks()
		
		navigationController?.navigationBarHidden = true
		setupTabBar()
		activeContainer = containerViews[0]
		showContainer(liveButton)

	}
	
	func fetchVideoLinks() {
//		let URL = NSURL(string: "http://107.170.151.144/api/videos")!
//		let mutableURLRequest = NSMutableURLRequest(URL: URL)
//		mutableURLRequest.HTTPMethod = "GET"
//		mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
//		let xAuth = NSUserDefaults.standardUserDefaults().stringForKey("X-Authenticate")
//		mutableURLRequest.setValue(xAuth, forHTTPHeaderField: "X-Authenticate")
//		Alamofire.request(mutableURLRequest)
//		.responseJSON { (request, response, jsonObject, error) in
//			//NSUserDefaults.standardUserDefaults().stringForKey("X-Authenticate")
//		}
	}
	
	func setupTabBar() {
		
		if DeviceType.IS_IPHONE_6 {
			tabViewHeight.constant = 57
			$.each(buttonWidth) { $0.constant = 85 }
		} else if DeviceType.IS_IPHONE_6P {
			tabViewHeight.constant = 64
			$.each(buttonWidth) { $0.constant = 106 }
		}
//		liveButton.titleLabel?.font = UIFont(name:"Sansus Webissimo", size:18)
//		sportButton.titleLabel?.font = UIFont(name:"Sansus Webissimo", size:18)
//		channelButton.titleLabel?.font = UIFont(name:"Sansus Webissimo", size:18)
//		UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name:"Sansus Webissimo", size:17)!, NSForegroundColorAttributeName: UIColor.whiteColor()], forState: UIControlState.Normal)
//		UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name:"MyriadPro-Regular", size:17)!, NSForegroundColorAttributeName: UIColor(0xff3b00)], forState: UIControlState.Selected)
	}
	
	@IBAction func showContainer(sender: UIButton) {
//		for button in tabButtons {
//			button.selected = false
//		}
//		sender.selected = !sender.selected

		tabImageView.image = UIImage(named: "\(sender.tag)_back")
		
//		activeContainer.hidden = true
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		switch(sender.tag) {
		case 1:
			if !self.liveVCLoaded {
				if let vc = storyboard.instantiateViewControllerWithIdentifier("LiveViewController") as? LiveViewController {
					let navVC = NavigationController(rootViewController: vc)
					self.loadViewController(navVC, inView: containerViews[sender.tag-1])
					self.liveVCLoaded = true
				}
			}
		case 2:
			if !self.sportVCLoaded {
				if let vc = storyboard.instantiateViewControllerWithIdentifier("SportViewController") as? SportViewController {
					let navVC = NavigationController(rootViewController: vc)
					self.loadViewController(navVC, inView: containerViews[sender.tag-1])
					self.sportVCLoaded = true
				}
			}
		case 3:
			if !self.channelVCLoaded {
				if let vc = storyboard.instantiateViewControllerWithIdentifier("ChannelViewController") as? ChannelViewController {
					let navVC = NavigationController(rootViewController: vc)
					self.loadViewController(navVC, inView: containerViews[sender.tag-1])
					self.channelVCLoaded = true
				}
			}
		case 4:
			if !self.searchVCLoaded {
				if let vc = storyboard.instantiateViewControllerWithIdentifier("SearchViewController") as? SearchViewController {
					let navVC = NavigationController(rootViewController: vc)
					self.loadViewController(navVC, inView: containerViews[sender.tag-1])
					self.searchVCLoaded = true
				}
			}
		case 5:
			if !self.settingsVCLoaded {
				if let vc = storyboard.instantiateViewControllerWithIdentifier("SettingsViewController") as? SettingsViewController {
					let navVC = NavigationController(rootViewController: vc)
					self.loadViewController(navVC, inView: containerViews[sender.tag-1])
					self.settingsVCLoaded = true
				}
			}
		default:
			println("wrong tag: \(sender.tag)")
		}
//		containerViews[sender.tag-1].hidden = false
		activeContainer = containerViews[sender.tag-1]
		view.bringSubviewToFront(activeContainer)
		view.bringSubviewToFront(tabView)
	}

	/**
	Add a child view controller to a view. Uses autoconstraints.

	:param: childViewController       the view controller to add as a child
	:param: containerView the view in current view controller to load the given view controller's view into
	*/
	func loadViewController(childViewController: UIViewController, inView containerView: UIView) {
		let childView = childViewController.view
		childView.translatesAutoresizingMaskIntoConstraints()
		childView.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
		loadViewController(childViewController, inView:containerView, withBounds: containerView.bounds)
	}
	
	/**
	Add a child view controller to a view. Uses provided bounds for the child's view.
	
	:param: childViewController       the view controller to add as a child
	:param: containerView the view in current view controller to load the given view controller's view into
	:param: bounds        the bounds of the view controller's view to be set
	*/
	func loadViewController(childViewController: UIViewController, inView containerView: UIView,
		withBounds bounds: CGRect) {
			let childView = childViewController.view
			childView.frame = bounds
			
			// Adding new view controller and its view to containerView
			self.addChildViewController(childViewController)
			containerView.addSubview(childView)
			
			// Finally notify the child view controller
			childViewController.didMoveToParentViewController(self)
	}
	
	/**
	Remove view controller and view from parents
	*/
	func removeFromParent() {
		self.willMoveToParentViewController(nil)
		self.view.removeFromSuperview()
		self.removeFromParentViewController()
	}
}