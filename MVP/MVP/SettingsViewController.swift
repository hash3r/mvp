//
//  SettingsViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/1/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

let data = [["Eddie Redmayne", "eddie_red@gmail.com"], ["Subscription settings" , "About MVP", "Terms & Privacy"]]

class SettingsViewController : BaseViewController, UITableViewDelegate, UITableViewDataSource {
	
	@IBOutlet weak var tableView: UITableView!
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Settings"
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		VideoLinkManager.sharedInstance.readContent()
	}
	
	// MARK: - Table view data source
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return section == 0 ? 0 : headerHeight
	}
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return data.count
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		return data[section].count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		if indexPath.section == 0 {
			let cell = tableView.dequeueReusableCellWithIdentifier("FirstCell", forIndexPath: indexPath) as! FirstCell
			if indexPath.row == 0 {
				cell.nameLabel.text = NSUserDefaults.standardUserDefaults().stringForKey("full_name") ?? data[indexPath.section][indexPath.row]
			} else if indexPath.row == 1 {
				cell.nameLabel.text = NSUserDefaults.standardUserDefaults().stringForKey("email") ?? data[indexPath.section][indexPath.row]
			}
			cell.selectionStyle = UITableViewCellSelectionStyle.None
			return cell
		} else if indexPath.section == 1 {
			let cell = tableView.dequeueReusableCellWithIdentifier("SecondCell", forIndexPath: indexPath) as! SecondCell
			cell.selectionStyle = UITableViewCellSelectionStyle.None
			cell.nameLabel.text = data[indexPath.section][indexPath.row]
			return cell
		} else if indexPath.section == 2 {
			let cell = tableView.dequeueReusableCellWithIdentifier("ThirdCell", forIndexPath: indexPath) as! ThirdCell
			cell.selectionStyle = UITableViewCellSelectionStyle.None
			if indexPath.row == 0 {
				cell.passField.text = NSUserDefaults.standardUserDefaults().stringForKey("password") ?? "password"
			}
			cell.nameLabel.text = data[indexPath.section][indexPath.row]
			return cell
		}
		return UITableViewCell()
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

	}
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, headerHeight))
		headerView.backgroundColor = UIColor.clearColor()
		return headerView
	}
	

	@IBAction func logoutTapped(sender: AnyObject) {
		NSUserDefaults.standardUserDefaults().removeObjectForKey("X-Authenticate")
		NSUserDefaults.standardUserDefaults().synchronize()
		if let delegate = UIApplication.sharedApplication().delegate as? AppDelegate {
			delegate.setupAuthController()
		}
	}
	
	@IBAction func monthlySubscriptionTapped(sender: AnyObject) {
		var alert = UIAlertController(title: "Confirm Your Subscription", message: "Do you want to subscribe to Renewable Subscription for 30 days for $9.99? This subscription will automatically renew until canceled", preferredStyle: UIAlertControllerStyle.Alert)
		alert.addAction(UIAlertAction(title: "Confirm", style: .Default, handler: { action in
		}))
		alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
		}))
		self.presentViewController(alert, animated: true, completion: nil)
	}
	
	func textFieldDidBeginEditing(textField: UITextField) {
		var point: CGPoint = CGPointMake(0, textField.frame.origin.y)
		let cell = textField.superview!.superview as! ThirdCell
		let indexPath = tableView.indexPathForCell(cell)
		tableView.setContentOffset(CGPointMake(0, CGFloat(indexPath!.row) * cell.frame.size.height), animated: true)
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		textField.resignFirstResponder()
		tableView.setContentOffset(CGPointZero, animated: true)
		return true
	}
}

class FirstCell : UITableViewCell {
	@IBOutlet weak var nameLabel: UILabel!
}

class SecondCell : FirstCell {
	
}

class ThirdCell : FirstCell {
	@IBOutlet weak var passField: UITextField!
}