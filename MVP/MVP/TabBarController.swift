//
//  TabBarController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
//	- (void)viewWillLayoutSubviews
//	{
//	CGRect tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
//	tabFrame.size.height = 80;
//	tabFrame.origin.y = self.view.frame.size.height - 80;
//	self.tabBar.frame = tabFrame;
//	}

	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = UIColor(0x191919)
		
		self.tabBar.backgroundImage = UIImage()
		self.tabBar.shadowImage = UIImage()

//		if DeviceType.IS_IPHONE_6P {
//			var tabFrame = tabBar.frame
//			tabFrame.size.height = 62
//			tabFrame.origin.y = view.frame.size.height - 62
//			tabBar.frame = tabFrame
//		}
		
//		if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P {
			var tabBarView = UIImageView(image:UIImage(named:"tab_bar_back")?.resizableImageWithCapInsets(UIEdgeInsetsMake(0,10,10,0)))
			tabBarView.frame = CGRectMake(0, 0, tabBar.frame.size.width, tabBar.frame.size.height);
			tabBar.addSubview(tabBarView)
//		}

		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		
		var liveViewController = storyboard.instantiateViewControllerWithIdentifier("LiveViewController") as! LiveViewController
		var liveNavController = NavigationController(rootViewController: liveViewController)

		let sportVC = storyboard.instantiateViewControllerWithIdentifier("SportViewController") as! SportViewController
		let sportsNavController = NavigationController(rootViewController: sportVC)

		let channelVC = storyboard.instantiateViewControllerWithIdentifier("ChannelViewController") as! ChannelViewController
		let channelNavController = NavigationController(rootViewController: channelVC)
		
		let searchVC = storyboard.instantiateViewControllerWithIdentifier("SearchViewController") as! SearchViewController
		let searchNavController = NavigationController(rootViewController: searchVC)

		var settingsViewController = storyboard.instantiateViewControllerWithIdentifier("SettingsViewController") as! SettingsViewController
		var settingsNavController = NavigationController(rootViewController: settingsViewController)
		
		var vcs = [liveNavController,
			sportsNavController,
			channelNavController,
			searchNavController,
			settingsNavController]
		
		self.viewControllers = vcs
		
		tabBar.barStyle = UIBarStyle.Black
		tabBar.translucent = false
		tabBar.clipsToBounds = true;
		
//		var tabBarItem1 = tabBar.items?[0] as! UITabBarItem
//		var tabBarItem2 = tabBar.items?[1] as! UITabBarItem
//		var tabBarItem3 = tabBar.items?[2] as! UITabBarItem
//		var tabBarItem4 = tabBar.items?[3] as! UITabBarItem
//		var tabBarItem5 = tabBar.items?[4] as! UITabBarItem
//
//		tabBarItem1.title = "Live"
//		tabBarItem1.setTitlePositionAdjustment(DeviceType.IS_IPHONE_5 ? UIOffset(horizontal: 5, vertical: -15) : UIOffset(horizontal: 5, vertical: -15))
//		tabBarItem1.image = UIImage()
//		tabBarItem1.selectedImage = UIImage(named: "tab_bar_selected")?.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 10, 0, 0), resizingMode: .Stretch)
//		tabBarItem1.imageInsets = DeviceType.IS_IPHONE_5 ? UIEdgeInsetsMake(-17.5, 0, 17.5, 0) : UIEdgeInsetsMake(-17.5, 0, 17.5, 0)
//		
//		tabBarItem2.title = "Sport"
//		tabBarItem2.setTitlePositionAdjustment(DeviceType.IS_IPHONE_5 ? UIOffset(horizontal: 15, vertical: -15) : UIOffset(horizontal: 18, vertical: -15))
//		tabBarItem2.image = UIImage()
//		tabBarItem2.selectedImage = UIImage(named: "tab_bar_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
//		tabBarItem2.imageInsets = DeviceType.IS_IPHONE_5 ? UIEdgeInsetsMake(-17.5, 0, 17.5, 0) : UIEdgeInsetsMake(-17.5, 0, 17.5, 0)
//		
//		tabBarItem3.title = "Channel"
//		tabBarItem3.setTitlePositionAdjustment(DeviceType.IS_IPHONE_5 ? UIOffset(horizontal: 24, vertical: -15) : UIOffset(horizontal: 31, vertical: -15))
//		tabBarItem3.image = UIImage()
//		tabBarItem3.selectedImage = UIImage(named: "tab_bar_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
//		tabBarItem3.imageInsets = DeviceType.IS_IPHONE_5 ? UIEdgeInsetsMake(-17.5, 0, 17.5, 0) : UIEdgeInsetsMake(-17.5, 0, 17.5, 0)
//		
//		tabBarItem4.image = UIImage(named: "search_icon")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
//		tabBarItem4.title = ""
//		tabBarItem4.selectedImage = UIImage(named: "search_icon_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
//		tabBarItem4.imageInsets = DeviceType.IS_IPHONE_5 ? UIEdgeInsetsMake(5, 21, -5, -21) : UIEdgeInsetsMake(9, 24, -9, -24)
//		
//		tabBarItem5.image = UIImage(named: "sett_icon")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
//		tabBarItem5.title = ""
//		tabBarItem5.selectedImage = UIImage(named: "sett_icon_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
//		tabBarItem5.imageInsets = DeviceType.IS_IPHONE_5 ? UIEdgeInsetsMake(5, 7, -5, -7) : UIEdgeInsetsMake(10, 9, -10, -9)
	}
	
	/**
	This is a delegate method. It is called when the device rotates orientation.
	
	:return: Always return YES.
	*/
	override func shouldAutorotate() -> Bool {
		return false
	}
	
	/**
	This is a delegate method. It is called to check the supported orientations.
	
	:return: The supported orientation.
	*/
	override func supportedInterfaceOrientations() -> Int {
		return Int(UIInterfaceOrientationMask.Portrait.rawValue)
	}

}

