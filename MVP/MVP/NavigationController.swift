//
//  NavigationController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
	
	override init(rootViewController: UIViewController) {
		super.init(rootViewController: rootViewController)
	}
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	/**
	This is a delegate method. It is called when the device rotates orientation.
	
	:return: Always return YES.
	*/
	override func shouldAutorotate() -> Bool {
		return false
	}
	
	/**
	This is a delegate method. It is called to check the supported orientations.
	
	:return: The supported orientation.
	*/
	override func supportedInterfaceOrientations() -> Int {
		return UIInterfaceOrientation.Portrait.rawValue
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
//		UINavigationBar.appearance().barStyle = UIBarStyle.BlackTranslucent
	}
}
