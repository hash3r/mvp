//
//  SportViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/1/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

//let sports = ["Basketball", "NFL", "Baseball", "NHL", "Soccer", "Golf", "Tennis", "Boxing", "Cycling", "Motorcycling"]

class SportViewController : BaseViewController {
	
	let sports = [SportType.Basketball, SportType.NFL, SportType.Baseball, SportType.NHL, SportType.Soccer, SportType.Golf, SportType.Tennis, SportType.Boxing, SportType.Cycling, SportType.Motorcycling, SportType.Cricket]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Sport"
	}
	
	// MARK: - Table view data source
//	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//		return section == 0 ? 0 : headerHeight
//	}
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		//		isTableViewInitialized = true
		//		return programs.count
		return sports.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("SportCell", forIndexPath: indexPath) as! SportCell
		cell.sportIcon.image = UIImage(named: "sp_icon_\(indexPath.row+1)")
		cell.sportLabel.text = sports[indexPath.row].description
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = storyboard.instantiateViewControllerWithIdentifier("SportDetailsViewController") as! SportDetailsViewController
//		vc.sportIndex = indexPath.row
		vc.selectedSport = sports[indexPath.row]
		self.navigationController?.pushViewController(vc, animated: true)
		tableView.deselectRowAtIndexPath(indexPath, animated: false)
	}
	
//	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//		let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, headerHeight))
//		headerView.backgroundColor = UIColor.clearColor()
//		return headerView
//	}
	
}

class SportCell: UITableViewCell {
	@IBOutlet weak var sportIcon: UIImageView!
	@IBOutlet weak var sportLabel: UILabel!
}