//
//  ChannelDetailsViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 7/1/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import HCYoutubeParser

class ChannelDetailsViewController : BaseViewController {
	var channelIndex: Int = 0
	var channel: Channel?
	
	@IBOutlet weak var channelFavoriteButton: UIButton!
	@IBOutlet weak var chnnalName: UILabel!
	@IBOutlet weak var channelDescription: UILabel!
	@IBOutlet weak var channelImage: UIImageView!
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var tabBarImage: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = channel?.name()
		chnnalName.text = channel?.name()
		channelDescription.text = channel?.description()
		channelImage.image = channel?.icon()
	}
	
//	override func viewWillLayoutSubviews() {
//		var flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
//		flowLayout.estimatedItemSize = CGSizeMake(200, 190)
//	}
	
//	override func viewWillAppear(animated: Bool) {
//		super.viewWillAppear(animated)
//		var flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
//		if DeviceType.IS_IPHONE_6 {
//			flowLayout.itemSize = CGSizeMake(flowLayout.itemSize.width * 1.17, flowLayout.itemSize.height * 1.17)
//		} else if DeviceType.IS_IPHONE_6P {
//			flowLayout.itemSize = CGSizeMake(flowLayout.itemSize.width * 1.96, flowLayout.itemSize.height * 1.96)
//		}
//	}

	@IBAction func tabBarButtonTapped(sender: UIButton) {
		tabBarImage.image = UIImage(named: "ch_tab_\(sender.tag)")
	}
	
	@IBAction func favortiteButtonTapped(sender: UIButton) {
		sender.selected = !sender.selected
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		if indexPath.row == 0 {
			var size = CGSizeMake(628/2, 593/2)
			if DeviceType.IS_IPHONE_6 {
				size = CGSizeMake(size.width * 1.17, size.height * 1.17)
			} else if DeviceType.IS_IPHONE_6P {
				size = CGSizeMake(size.width * 1.94, size.height * 1.94)
			}
			return size
		} else {
			var size = CGSizeMake(628/2, 240/2)
			if DeviceType.IS_IPHONE_6 {
				size = CGSizeMake(size.width * 1.17, size.height * 1.17)
			} else if DeviceType.IS_IPHONE_6P {
				size = CGSizeMake(size.width * 1.94, size.height * 1.94)
			}
			return size
		}
	}

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return channel?.type == .AFN || channel?.type == .NBA ? 4 : 1
	}

	// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		if indexPath.item == 0 {
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CurrentCell", forIndexPath: indexPath) as! CurrentCell
			cell.bgImage.image = channel?.videoImage()
			cell.videoDescriptionLabel.text = channel?.videoDescription()
			return cell
		} else {
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("UpcomingCell", forIndexPath: indexPath) as! UpcomingCell
			cell.bgImage.image = UIImage(named: "ch_up_cell_\(indexPath.item)")
			return cell
		}
	}
	
	@IBAction func playVideo(sender: AnyObject) {
		var vc = VideoPlayerViewController()
		vc.view.frame = UIScreen.mainScreen().bounds
//		basket
		if let video = HCYoutubeParser.h264videosWithYoutubeURL(VideoLinkManager.sharedInstance.linkForChannelType(channel!.type)) {
			if let mediumUrl = video["medium"] as? String {
				vc.url = NSURL(string:mediumUrl)!
			}
		}
		if let url = vc.url {
			self.presentViewController(vc, animated: false, completion: nil)
		} else {
			var alert = UIAlertController(title: "Wrong URL", message: "This video is not available", preferredStyle: UIAlertControllerStyle.Alert)
			alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
			}))
			self.presentViewController(alert, animated: true, completion: nil)
		}
//		vc.url = NSURL(string: "http://107.170.151.144/api/videos/11/video_file_one_off/6753c27db92a9b7065ec8022cf90aab4eb8f9b3a")!
//		self.presentViewController(vc, animated: false, completion: nil)
	}

}

class CurrentCell: UpcomingCell {
	@IBOutlet weak var videoDescriptionLabel: UILabel!
}

class UpcomingCell: UICollectionViewCell {
	@IBOutlet weak var remindButton: UIButton!
	@IBOutlet weak var bgImage: UIImageView!
	@IBAction func remindButtonTapped(sender: UIButton) {
		sender.selected = !sender.selected
	}
}